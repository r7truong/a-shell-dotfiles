" Using pathogen to manage plugins
execute pathogen#infect()
syntax on
filetype plugin indent on

" Use true 24-bit colors (required for the vim-hexokinase plugin to work)
set termguicolors
colorscheme codesmell_dark

" Enable indent guides
let g:indent_guides_enable_on_vim_startup = 1
let g:rainbow_active = 1

set hlsearch
set incsearch

" Show existing tab with 4 spaces width
set tabstop=4
" When indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
set autoindent

" Show current line number
set nu
" Show other line numbers relative to the current line number
set relativenumber
" Start scrolling when moving past 10 lines from the top or bottom - keeps the cursor near the centre of the screen
set scrolloff=15

" Case insensitive search unless it contains a capital letter
set ignorecase
set smartcase

" Stop creating swap files
set noswapfile

let mapleader = " "

" Save
nnoremap <leader>fs :w<Cr>

let g:EasyMotion_smartcase = 1
nnoremap s <Plug>(easymotion-bd-f)


" Remap scrolling list of completion candidates
inoremap <expr> <M-j> ((pumvisible())?("\<C-n>"):("M-j"))
inoremap <expr> <M-k> ((pumvisible())?("\<C-p>"):("M-k"))
inoremap <C-g> <Esc>
cnoremap <C-g> <Esc>


" Splits management
" Create a horizontal split
nnoremap <leader>w :vsplit<Cr>
" Kill current split
nnoremap <leader>k :q<Cr>
" Focus next window
nnoremap <leader>a :wincmd w<Cr>


" Switch buffers
nnoremap - :bprevious<Cr>
nnoremap _ :bnext<Cr>
nnoremap <leader>bh :e ~/Documents<Cr>
nnoremap <leader>bn :e ~/Documents/notes<Cr>
nnoremap <leader>bf :e 
set autochdir

" File Explorer
" remove annoying banner at the start
let g:netrw_banner=0
" don't create history file
let g:netrw_dirhistmax = 0
nnoremap <leader>d :Explore<Cr>

function! NetrwMapping()
  nmap <buffer> h <Plug>NetrwBrowseUpDir
  nmap <buffer> l <CR>
  nmap <buffer> - :bprevious<Cr>
endfunction

augroup netrw_mapping
  autocmd!
  autocmd filetype netrw call NetrwMapping()
augroup END


" Hydra
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
set timeoutlen=0


" Snippets
let g:UltiSnipsExpandTrigger       = '<Tab>'
let g:UltiSnipsJumpForwardTrigger  = '<Tab>'
let g:UltiSnipsJumpBackwardTrigger = '<S-Tab>'


" Finding files
" Find from current directory
nnoremap <leader>ff :find <Tab>
nnoremap <leader>fh :find ~/Documents/**<Tab>
nnoremap <leader>fn :find ~/Documents/notes/**<Tab>
nnoremap <A-x> :
nnoremap <leader>of :e ~/Documents/notes/


" Include wrapped lines in navigation
set wrap
set linebreak
set nolist
set textwidth=0
set wrapmargin=0
set formatoptions-=t
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'
nnoremap <expr> $ v:count ? '$' : 'g$'
nnoremap <expr> 0 v:count ? '0' : 'g0'


" Org Mode
let g:org_clean_folds = 1
let g:org_list_alphabetical_bullets = 1
autocmd FileType org setlocal tabstop=2 shiftwidth=2 foldlevel=99
nnoremap <Tab> za


" Snippets
"" Free up tab key
let g:UltiSnipsExpandTrigger = "<f5>"
let g:UltiSnipsJumpForwardTrigger = "<Cr>"
let g:UltiSnipsJumpBackwardTrigger = '<S-Cr>'

" Completion
let g:mucomplete#no_mappings  = 1
set shortmess+=c
set completeopt-=preview
set completeopt+=menuone,noinsert
let g:mucomplete#enable_auto_at_startup = 1

"" Sources
let g:mucomplete#chains = { 'default':
            \ [ 'ulti','omni','tags','keyn','keyp','path','line','dict','uspl'] }


"" Autoexpand if completion is a snippet
inoremap <silent> <expr> <plug><MyTab>
    \ mucomplete#ultisnips#expand_snippet("\<Tab>")
imap <Tab> <plug><MyTab>
